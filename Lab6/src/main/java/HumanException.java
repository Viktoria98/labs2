public class HumanException extends Exception {
    public HumanException(String excText){
        super(excText);
    }
}
