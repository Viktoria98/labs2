import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class StudentTest {
    @Test
    public void testConstructor() throws HumanException {
        Student obj = new Student("Abc", "Abc", "Abc", 1, "Gg");
        assertNotNull(obj);
    }

    @Test
    public void testSettersAndGetters() throws HumanException {
        Student obj = new Student("abc", "ad", "s", 10, "Ff");
        obj.setFacultyName("Gg");
        assertEquals(obj.getFacultyName(), "Gg");
    }
}
