import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * Класс Human с полями: фамилия, имя, отчество, возраст и методами:
 * конструкторы, геттеры/сеттеры, equals и hashCode.
 */
public class Human implements Comparable<Human>{
    private String name;
    private String lastName;
    private String surname;
    private int age;

    public Human(String name, String lastName , String surname, int age) throws HumanException {
        if((name == null || name.isEmpty()) || (lastName == null || lastName.isEmpty())
                || (surname == null || surname.isEmpty())){
            throw new HumanException("Empty name or last name or surname");
        }
        else if(age <= 0){
            throw new HumanException("Incorrect age");
        }
        this.lastName = lastName;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public void setLastName(String lastName) throws HumanException {
        if(lastName == null || lastName.isEmpty()){
            throw new HumanException("Empty lastname");
        }
        this.lastName = lastName;
    }

    public void setName(String name) throws HumanException {
        if(name == null || name.isEmpty()){
            throw new HumanException("Empty name");
        }
        this.name = name;
    }

    public void setSurname(String surname) throws HumanException {
        if(surname == null || surname.isEmpty()){
            throw new HumanException("Empty surname");
        }
        this.surname = surname;
    }

    public void setAge(int age) throws HumanException {
        if(age <= 0){
            throw new HumanException("Incorrect age");
        }
        this.age = age;
    }

    public String getLastName() {
        return lastName;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        return age == human.age && Objects.equals(lastName, human.lastName)
                && Objects.equals(name, human.name) && Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, name, surname, age);
    }

    @Override
    public int compareTo(@NotNull Human o) {
        if(lastName.compareTo(o.getLastName()) == 0){
            if (name.compareTo(o.getName()) == 0){
                if(surname.compareTo(o.getSurname()) == 0){
                    return 0;
                }
                return surname.compareTo(o.getSurname());
            }
            return name.compareTo(o.getName());
        }
        return lastName.compareTo(o.getLastName());
    }
}
