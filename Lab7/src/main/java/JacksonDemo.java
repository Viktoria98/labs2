import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class JacksonDemo {
    /**
     * 8.Метод сериализации объекта типа House в строку(использующий data binding)
     */
    public static String writeJsonString(House house) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(house);
    }

    /**
     * 8.Метод десериализации строки в объект типа House(использующий data binding)
     */
    public static House readJsonString(String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, House.class);
    }

    /**
     * 9. Метод, сравнивает две json-строки на равенство.
     */
    public static boolean jsonEqual(String json1, String json2) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode js1 = mapper.readTree(json1);
        JsonNode js2 = mapper.readTree(json2);
        return js1.equals(js2);
    }

    /**
     * 10.Метод сериализации объекта типа Person в строку(использующий Streaming API)
     */
    public static void serializePerson(@NotNull Person person, OutputStream os) throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try(JsonGenerator generator = jsonFactory.createGenerator(os, JsonEncoding.UTF8)){
            generator.writeStartObject();
            generator.writeStringField("fullname", person.getName() + " " + person.getLastName() + " "
                    + person.getSurName());
            generator.writeNumberField("day", person.getDay());
            generator.writeNumberField("month", person.getMonth());
            generator.writeNumberField("year", person.getYear());
            generator.writeEndObject();
        }
    }



    /**
     * 10.Метод десериализации строки в объект типа Person(использующий Streaming API)
     */
    @NotNull
    @Contract("_ -> new")
    public static Person deserializePerson(InputStream is) throws IOException, PersonException {
        JsonFactory jsonFactory = new JsonFactory();
        try(JsonParser parser = jsonFactory.createParser(is)) {
            JsonToken token = parser.nextToken();
            String[] fio = parser.getText().split(" ");
            String name = fio[0];
            String lastName = fio[1];
            String surName = fio[2];
            token = parser.nextToken();
            int day = parser.getIntValue();
            token = parser.nextToken();
            int month = parser.getIntValue();
            token = parser.nextToken();
            int year = parser.getIntValue();
            return new Person(name, lastName, surName, day, month, year);
        }
    }



    /**
     * 10.Метод сериализации объекта типа Person в строку(использующий Streaming API)
     */
    public static void serializeFlat(@NotNull Flat flat, OutputStream os) throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        try(JsonGenerator generator = jsonFactory.createGenerator(os, JsonEncoding.UTF8)){
            generator.writeStartObject();
            generator.writeNumberField("apartmentNumber", flat.getApartmentNumber());
            generator.writeNumberField("area", flat.getArea());
            generator.writeArrayFieldStart("owners");
            for(Person temp: flat.getOwners()){
                serializePerson(temp, os);
            }
            generator.writeEndArray();
            generator.writeEndObject();
        }
    }

    @NotNull
    @Contract("_ -> new")
    public static Flat deserializeFlat(InputStream is) throws IOException, PersonException {
        /*
        JsonFactory jsonFactory = new JsonFactory();
        try(JsonParser parser = jsonFactory.createParser(is)) {
            JsonToken token = parser.nextToken();
            int apartmentNumber = parser.getIntValue();
            token = parser.nextToken();
            int area = parser.getIntValue();
            token = parser.nextToken();
            ArrayList<Person> arr;
            while (parser.nextToken() != JsonToken.END_ARRAY){
                arr.add(deserializePerson())
            }
            return new Flat();
        }
        */
        return new Flat();
    }


}
