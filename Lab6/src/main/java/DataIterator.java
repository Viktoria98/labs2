import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Итератор, который для объекта класса Data перебирает все числа во всех его
 * группах
 */
public class DataIterator implements Iterator<Integer> {

    private Data iterableData;
    private int readIndex;
    private int groupIndex;

    DataIterator(Data data){
        this.iterableData = data;
        this.readIndex = 0;
        this.groupIndex = 0;
    }

    @Override
    public boolean hasNext() {
        return iterableData.getLength() > groupIndex && iterableData.getGroup(groupIndex).getLength() > readIndex;
    }

    @Override
    public Integer next() {
        if(!hasNext()){throw new NoSuchElementException();}
        else {
            int res = iterableData.getGroup(groupIndex).getElem(readIndex);
            if (readIndex + 1 == iterableData.getGroup(groupIndex).getLength()) {
                groupIndex++;
                readIndex = 0;
            }
            else {
                readIndex++;
            }
            return res;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove");
    }


}
