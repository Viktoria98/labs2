/**
 * Класс, реализующий интерфейс «функция одного вещественного аргумента, определенная на отрезке [a; b]»
 * Функция: f(x) = (Ax + B) / (Cx + D)
 */

public class RationalFraction implements IFunctionOneArgument {

    private LinearFunction numerator;
    private LinearFunction denominator;

    public RationalFraction(LinearFunction numerator, LinearFunction denominator) throws FunctionException {
        double epsilon = 10e-8;
        if(Math.abs(numerator.getStart() - denominator.getStart()) >= epsilon ||
                Math.abs(numerator.getEnd() - denominator.getEnd()) >= epsilon){
            throw new FunctionException("Области определения не совпадают");
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public RationalFraction(double a, double b, double c, double d, double start, double end)
            throws FunctionException {
        numerator = new LinearFunction(a, b, start, end);
        denominator = new LinearFunction(c, d, start, end);
    }

    @Override
    public double getStart() {
        return denominator.getStart();
    }

    @Override
    public double getEnd() {
        return denominator.getEnd();
    }

    @Override
    public double getValue(double x) throws FunctionException {
        double epsilon = 10e-8;
        if(Math.abs(denominator.getValue(x)) < epsilon){
            throw new FunctionException("Знаменатель равен 0. Получить значение невозможно");
        }
        return numerator.getValue(x)/denominator.getValue(x);
    }

}
