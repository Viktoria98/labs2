import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DataDemoTest {

    @Test
    public void getAllTest(){
        Data test = new Data("Test data", new Group(100, 1, 2, 3),
                new Group(12, 3, 5), new Group(11, 22, 7, 90, 5));

        List<Integer> res = new ArrayList<>(Arrays.asList(1,2,3,3,5,22,7,90,5));

        assertIterableEquals(res, DataDemo.getAll(test));

    }


}
