import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 2*.Класс StreamApiDemo, производный от LambdaDemo. Содержит следующие, использующие Stream API, лямбда-выражения:
 */
public class StreamApiDemo extends LambdaDemo {
    /**
     *1.В списке объектов удаляет все значения null.
     */
    public static Function<List<Object>, List<Object>> deleteNull =
            x -> x.stream().filter(Objects::nonNull).collect(Collectors.toList());

    /**
     *2.Во множестве целых чисел находит количество положительных значений.
     */
    public static Function<Set<Integer>, Long> countOfPositive = x ->
        Optional.ofNullable(x).map(z -> z.stream().filter(w -> w > 0).count()).orElse(null);

    /**
     *3.В списке объектов получает последние три элемента.
     */
    public static Function<List<Object>, List<Object>> threeLastElems = x ->
        Optional.ofNullable(x).filter(s -> s.size() >= 3)
                .map(z -> z.stream().skip(z.size()-3).collect(Collectors.toList())).orElse(null);

    /**
     *4.В списке целых чисел получает первое четное число или значение null, если в списке
     *  нет четных чисел.
     */
    public static Function<List<Integer>, Integer> firstEvenNum = x ->
        Optional.ofNullable(x).flatMap(s -> s.stream().filter(z -> z % 2 == 0).findFirst())
                .orElse(null);

    /**
     *5.По массиву целых чисел строит список квадратов элементов массива без
     *  повторений.
     */
    public static Function<Integer[], List<Integer>> squareOfNumber = x ->
            Optional.ofNullable(x).filter(z -> z.length > 0)
                    .map(z -> Arrays.stream(z).map(r -> r * r).distinct().collect(Collectors.toList()))
                    .orElse(null);

    /**
     *6.По списку строк строит новый список, содержащий все непустые строки исходного
     *  списка, упорядоченные по возрастанию.
     */
    public static Function<List<String>, List<String>> sortedNonNullStrings = x->
        Optional.ofNullable(x).map(z->z.stream().filter(Objects::nonNull)
                .filter(w -> !w.isEmpty()).sorted().collect(Collectors.toList())).orElse(null);

    /**
     *7.Множество строк превращает в список, упорядоченный по убыванию.
     */
    public static Function<Set<String>, List<String>> descendingStringSort = x ->
           Optional.ofNullable(x).map(z -> z.stream().filter(Objects::nonNull).
                   filter(w -> !w.isEmpty()).sorted(Comparator.reverseOrder()).collect(Collectors.toList())
           ).orElse(null);


    /**
     *8.По множеству целых чисел вычисляет сумму квадратов его элементов
     */
    public static Function<Set<Integer>, Integer> sumOfNumbersSquares = x ->
        Optional.ofNullable(x).map(z -> z.stream().reduce(0, (res, s)-> res + s*s)).orElse(null);

    /**
     *9.По коллекции людей вычисляет максимальную возраст человека
     */
    public static Function<Collection<Human>, Integer> maxAge = x ->
        Optional.ofNullable(x).filter(z -> z.size() != 0)
                .map(w -> w.stream().max(Comparator.comparingInt(Human::getAge)).get().getAge())
                .orElse(null);

    /**
     *10.Сортирует коллекцию людей сперва по полу, затем — по возрасту.
     */
    public static Function<Collection<Human>, Collection<Human>> genderAndAgeSorted = x ->
            Optional.ofNullable(x).filter(z -> z.size() != 0)
                    .map(w -> w.stream().sorted(Comparator.comparingInt(Human::getAge))
                            .sorted(new GenderComparator()).collect(Collectors.toList())).orElse(null);


}
