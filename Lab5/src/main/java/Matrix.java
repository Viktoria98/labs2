import java.util.Arrays;
import java.util.Objects;

/**
 * Класс Matrix (квадратная матрица произвольного вида). Размерность
 * матрицы N задается при создании объекта и в дальнейшем не меняется.
 * Матрицу хранится в виде одномерного массива длины NxN
 */
public class Matrix implements IMatrix {
    private double[] matrix;
    private int matrixSize;
    private double determinant;
    private boolean haveRightDeterminant;

    public Matrix() {
        matrix = new double[1];
        matrixSize = 1;
        determinant = 0;
        haveRightDeterminant = true;
    }

    public int getMatrixSize() {
        return matrixSize;
    }

    public Matrix(int N) throws MatrixException {
        if (N <= 0) {
            throw new MatrixException("Некоректный размер матрицы", N);
        }
        matrix = new double[N * N];
        matrixSize = N;
        determinant = 0;
        haveRightDeterminant = true;
    }

    public Matrix(double[] matrix, int matrixSize) throws MatrixException {
        if(matrix == null){
            throw new MatrixException("Передан null");
        }
        else if (matrixSize <= 0) {
            throw new MatrixException("Некоректный размер матрицы", matrixSize);
        }
        this.matrix = matrix;
        this.matrixSize = matrixSize;
    }

    @Override
    public double getElem(int i, int j) throws MatrixException {
        if (i >= matrixSize || i < 0) {
            throw new MatrixException("Некоректный индекс массива", i);
        }
        else if (j >= matrixSize || j < 0) {
            throw new MatrixException("Некоректный размер матрицы", j);
        }
        return matrix[i * matrixSize + j];
    }

    @Override
    public void editElem(int i, int j, double elem) throws MatrixException {
        if (i >= matrixSize || i < 0) {
            throw new MatrixException("Некоректный индекс массива", i);
        }
        else if (j >= matrixSize || j < 0) {
            throw new MatrixException("Некоректный размер матрицы", j);
        }
        haveRightDeterminant = false;
        matrix[i * matrixSize + j] = elem;
    }

    private Matrix copyMatrix() throws MatrixException {
        double[] tempMatrix = new double[matrixSize * matrixSize];
        System.arraycopy(matrix, 0, tempMatrix, 0, matrixSize * matrixSize);
        return new Matrix(tempMatrix, matrixSize);
    }

    /**
     * Вычисляет определитель матрицы  методом Гаусса.
     * @return определитель
     * @throws MatrixException - если выброшено в одном из методов, используемых в классе
     */

    @Override
    public double getDeterminant() throws MatrixException{
        if (haveRightDeterminant) {
            return determinant;
        }
        determinant = 1;
        double tempElem;
        Matrix tempMatrix = this.copyMatrix();
        for (int j = 0; j < matrixSize; j++) {
            tempElem = tempMatrix.getElem(j, j);
            if (tempElem == 0) {
                for (int i = j; i < matrixSize; i++) {
                    tempElem = tempMatrix.getElem(i, j);
                    if (tempElem != 0) {
                        tempMatrix.swapRows(i, j);
                        determinant *= -1;
                        break;
                    }
                    tempElem = tempMatrix.getElem(matrixSize - 1, j);
                    if (i == matrixSize - 1 && tempElem == 0) {
                        determinant = 0;
                        haveRightDeterminant = true;
                        return determinant;
                    }
                }
            }
            for (int i = j + 1; i < matrixSize; i++) {
                tempMatrix.subtract(i, j, tempMatrix.getElem(i, j) / tempMatrix.getElem(j, j));
            }
        }
        for (int i = 0; i < matrixSize; i++) {
            determinant *= tempMatrix.getElem(i, i);
        }
        haveRightDeterminant = true;
        return determinant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Matrix matrix1 = (Matrix) o;
        return matrixSize == matrix1.matrixSize && Arrays.equals(matrix, matrix1.matrix);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(matrixSize);
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }

    private void swapRows(int str1, int str2) throws MatrixException {
        if (str1 < 0 || str1 >= matrixSize) {
            throw new MatrixException("Некоректный номер строки матрицы", str1);
        }
        else if (str2 < 0 || str2 >= matrixSize) {
            throw new MatrixException("Некоректный номер строки матрицы", str2);
        }
        else {
            double tempElem;
            for (int i = 0; i < matrixSize; i++) {
                tempElem = matrix[str1 * matrixSize + i];
                matrix[str1 * matrixSize + i] = matrix[str2 * matrixSize + i];
                matrix[str2 * matrixSize + i] = tempElem;
            }
        }
    }

    private void subtract(int i, int j, double coeff) {
        for (int k = 0; k < matrixSize; k++) {
            matrix[i * matrixSize + k] -= coeff * matrix[j * matrixSize + k];
        }
    }

}
