import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class StreamApiDemoTest {

    @Test
    void testDeleteNull(){
        List<Object> list = new ArrayList<>(5);
        List<Object> res = new ArrayList<>(3);

        list.add(" ");
        res.add(" ");

        list.add(3);
        res.add(3);

        list.add(null);
        list.add(null);

        list.add('s');
        res.add('s');

        assertEquals(res, StreamApiDemo.deleteNull.apply(list));

    }

    @Test
    void testCountOfPositive(){
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(-2);
        set.add(3);
        set.add(-4);
        set.add(5);

        assertNull(StreamApiDemo.countOfPositive.apply(null));
        assertEquals(3, StreamApiDemo.countOfPositive.apply(set));
    }

    @Test
    void testThreeLastElems(){
        List<Object> list1 = new ArrayList<>(2);
        List<Object> list2 = new ArrayList<>(5);
        List<Object> res2 = new ArrayList<>(2);

        list1.add(3);
        list1.add(12);

        list2.add('c');
        list2.add("das");
        list2.add(12);
        res2.add(12);
        list2.add(42);
        res2.add(42);
        list2.add(9);
        res2.add(9);

        assertNull(StreamApiDemo.threeLastElems.apply(list1));
        assertEquals(res2, StreamApiDemo.threeLastElems.apply(list2));
    }

    @Test
    void testFirstEvenNum(){
        List<Integer> list = new ArrayList<>(3);
        List<Integer> list1 = new ArrayList<>(5);

        list.add(1);
        list.add(13);
        list.add(61);

        list1.add(43);
        list1.add(22);
        list1.add(83);
        list1.add(58);
        list1.add(11);

        assertNull(StreamApiDemo.firstEvenNum.apply(list));
        assertEquals(22, StreamApiDemo.firstEvenNum.apply(list1));
    }

    @Test
    void testSquareOfNumber(){
        Integer [] arr = new Integer[]{2, 11, 17, 29, 17, 9, 11, 6};
        List<Integer> res = Arrays.asList(4, 121, 289, 841, 81, 36);

        assertEquals(res, StreamApiDemo.squareOfNumber.apply(arr));
    }

    @Test
    void testSortedNonNullStrings(){
        List<String> list = new ArrayList<>(7);
        List<String> res = new ArrayList<>(5);

        list.add("");
        list.add("fdac");
        list.add("zfsf");
        list.add(null);
        list.add("bdcf");
        list.add("avsd");
        list.add("favf");

        res.add("avsd");
        res.add("bdcf");
        res.add("favf");
        res.add("fdac");
        res.add("zfsf");

        assertEquals(res, StreamApiDemo.sortedNonNullStrings.apply(list));

    }

    @Test
    void testDescendingStringSort(){
        Set<String> set = new HashSet<>(7);
        List<String> res = new ArrayList<>(5);

        set.add("");
        set.add("fdac");
        set.add("zfsf");
        set.add("bdcf");
        set.add(null);
        set.add("avsd");
        set.add("favf");

        res.add("zfsf");
        res.add("fdac");
        res.add("favf");
        res.add("bdcf");
        res.add("avsd");

        assertEquals(res, StreamApiDemo.descendingStringSort.apply(set));

    }

    @Test
    void testSumOfNumbersSquares(){
        Set<Integer> numbers = new HashSet<>();
        numbers.add(7);
        numbers.add(14);
        numbers.add(20);
        numbers.add(15);
        numbers.add(9);

       assertEquals(951, StreamApiDemo.sumOfNumbersSquares.apply(numbers));

    }

    @Test
    void testMaxAge() throws HumanException {
        List<Human> list = new ArrayList<>(4);
        list.add(new Human("da", "mdasd", "fdve", 23, Human.Gender.M));
        list.add(new Human("da", "hfdhd", "bnf", 17, Human.Gender.M));
        list.add(new Human("bgtny", "gs", "terb", 54, Human.Gender.F));
        list.add(new Human("nhg", "grtg", "fnvbe", 31, Human.Gender.F));
        list.add(new Human("erve", "fdgd", "nbc", 25, Human.Gender.M));

        Set<Human> set = new HashSet<>(3);
        set.add(new Human("ij", "qdx", "gtv", 44, Human.Gender.F));
        set.add(new Human("fv", "bgf", "tg", 11, Human.Gender.F));
        set.add(new Human("cd", "nhbg", "fenb", 20, Human.Gender.M));

        assertEquals(54, StreamApiDemo.maxAge.apply(list));
        assertEquals(44, StreamApiDemo.maxAge.apply(set));

    }

    @Test
    void testGenderAndAgeSorted() throws HumanException {
        List<Human> list = new ArrayList<>(7);
        List<Human> res = new ArrayList<>(7);

        Human human1 = new Human("A", "T", "W", 15, Human.Gender.M);
        Human human2 = new Human("B", "T", "S", 25, Human.Gender.F);
        Human human3 = new Human("C", "F", "A", 20, Human.Gender.F);
        Human human4 = new Human("D", "H", "C", 27, Human.Gender.M);
        Human human5 = new Human("E", "K", "X", 11, Human.Gender.F);
        Human human6 = new Human("F", "L", "K", 24, Human.Gender.M);
        Human human7 = new Human("G", "Z", "Y", 23, Human.Gender.F);

        list.add(human1);
        list.add(human2);
        list.add(human3);
        list.add(human4);
        list.add(human5);
        list.add(human6);
        list.add(human7);

        res.add(human5);
        res.add(human3);
        res.add(human7);
        res.add(human2);
        res.add(human1);
        res.add(human6);
        res.add(human4);

        assertEquals(res, StreamApiDemo.genderAndAgeSorted.apply(list));

    }

}
