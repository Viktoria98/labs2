import java.util.Objects;

/**
 * 2.Производный от класса Human класс Student с добавленными полями университет, факультет,
 * специальность
 */
public class Student extends Human{
    private String faculty;
    private String university;
    private String specialisation;

    public Student(String lastName, String name, String surname, int age, Gender gender,
                   String faculty, String university, String specialisation) throws HumanException {
        super(lastName, name, surname, age, gender);
        if(faculty == null || faculty.isEmpty() || university == null || university.isEmpty()
                || specialisation == null || specialisation.isEmpty()){
            throw new HumanException("Empty or null faculty/university/specialisation");
        }
        this.faculty = faculty;
        this.university = university;
        this.specialisation = specialisation;
    }

    public String getFaculty(){
        return faculty;
    }

    public void setFaculty(String faculty) throws HumanException {
        if(faculty == null || faculty.isEmpty()){ throw new HumanException("Empty or null faculty");}
        this.faculty = faculty;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) throws HumanException {
        if(university == null || university.isEmpty()){throw new HumanException("Empty or null university");}
        this.university = university;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) throws HumanException  {
        if(specialisation == null || specialisation.isEmpty()){throw new HumanException("Empty or null specialisation");}
        this.specialisation = specialisation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Student student = (Student) o;
        return Objects.equals(faculty, student.faculty) && Objects.equals(university, student.university) && Objects.equals(specialisation, student.specialisation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), faculty, university, specialisation);
    }
}
