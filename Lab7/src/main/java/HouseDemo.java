import org.jetbrains.annotations.NotNull;
import java.io.*;

/**
 * 6.Cервисный класс с методами, которые сериализуют и десериализуют объект типа
 * House в заданный поток средствами Java.
 */
public class HouseDemo {
    public static void serializeHouse(@NotNull House house, String fileName) throws IOException {
        try (ObjectOutput out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))){
            house.serialize(out);
        }
    }

    public static House deserializeHouse(String fileName) throws IOException, ClassNotFoundException {
        try (ObjectInput in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fileName)))){
            return (House) in.readObject();
        }
    }
}
