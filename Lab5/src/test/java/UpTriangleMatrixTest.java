import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UpTriangleMatrixTest {

    @Test
    void testConstructor() throws MatrixException {
        assertThrows(MatrixException.class,()->new UpTriangleMatrix(3, 2,2,3,1,2));
        UpTriangleMatrix matrix = new UpTriangleMatrix(4, 1, 23, 45, 21, 11, 42, 56, 11, -1, -3);
    }

    @Test
    void testEditElem() throws MatrixException {
        UpTriangleMatrix matrix = new UpTriangleMatrix(4, 1, 23, 45, 21, 10, 42, 56, 90, -1, -3);
        assertThrows(MatrixException.class, ()->matrix.editElem(1,0, 9));
        matrix.editElem(0,2,11);
        assertEquals(11, matrix.getElem(0,2));
    }
}