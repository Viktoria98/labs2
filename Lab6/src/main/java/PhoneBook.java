import java.util.*;

/**
 * Класс PhoneBook (телефонная книга).
 * Книга представляет собой отображение
 * человека в список его номеров телефонов (номер телефона — строка).
 *
 */
public class PhoneBook {
    private Map<Human, List<String>> contacts;

    public Map<Human, List<String>> getContacts() {
        return contacts;
    }

    public PhoneBook(){
        contacts = new HashMap<>();
    }

    /**
     * Метод добавления телефона
     */
    public void addPhone(Human human, String phone){
        if(!contacts.containsKey(human)) {
            contacts.put(human, new ArrayList<>());
        }
        contacts.get(human).add(phone);
    }

    /**
     * Метод удаления телефона
     */
    public void deletePhone(Human human, String phone) throws PhoneBookException {
        if(!contacts.containsKey(human)){
            throw new PhoneBookException("Phone book don't contain this human");
        }
        contacts.get(human).remove(phone);

    }

    /**
     * Метод получения списка телефонов по человеку
     */
    public List<String> getPhones(Human human) throws PhoneBookException {
        if(!contacts.containsKey(human)){
            throw new PhoneBookException("Phone book don't contain this human");
        }
        return contacts.get(human);
    }

    /**
     *  Метод нахождения человека по номеру телефона
     */
    public Human searchHuman(String phone) throws PhoneBookException {
        Set<Human> keySet = contacts.keySet();
        for(Human temp: keySet){
            for(String tempPhone: contacts.get(temp)){
                if(tempPhone.equals(phone)){ return temp; }
            }
        }
        throw new PhoneBookException("Phone book don't contain this phone");
    }

    /**
     * Метод нахождения всех людей с их телефонами по началу фамилии человека
     * (результат – новое отображение такой же структуры, но содержащее только отобранные
     * записи).
     */
    public Map<Human, List<String>> getContactsPeopleWithSameBeginLastname(String lastNamePrefix){
        Map<Human, List<String>> res = new HashMap<>();
        for(Human temp: contacts.keySet()){
            if(temp.getLastName().startsWith(lastNamePrefix)){
                res.put(temp, contacts.get(temp));
            }
        }
        return res;
    }

}
