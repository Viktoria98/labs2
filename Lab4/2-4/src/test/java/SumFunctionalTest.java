import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumFunctionalTest {

    @Test
    void getValue() throws FunctionException {
        SumFunctional test = new SumFunctional();
        LinearFunction func = new LinearFunction(2, 5, 2,10);
        assertThrows(FunctionException.class,()->test.getValue(null));
        assertEquals(51,test.getValue(func));
    }
}