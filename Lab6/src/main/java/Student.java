/**
 * Класс Student, производный от Human, новое поле — название факультета.
 */
public class Student extends Human {
    private String facultyName;
    public Student(String lastName, String name, String surname, int age, String facultyName) throws HumanException {
        super(lastName, name, surname, age);
        if(facultyName == null || facultyName.isEmpty()){
            throw new HumanException("Empty faculty name");
        }
        this.facultyName = facultyName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) throws HumanException {
        if(facultyName == null || facultyName.isEmpty()){
            throw new HumanException("Empty faculty name");
        }
        this.facultyName = facultyName;
    }
}
