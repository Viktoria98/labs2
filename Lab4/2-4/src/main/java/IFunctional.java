/**
 * Параметризованный интерфейс
 * «Функционал от одного аргумента» c методом вычисления значения функционала от
 * заданной функции. Параметром является тип функций
 * @param <T>
 */

public interface IFunctional<T extends IFunctionOneArgument> {
    double getValue(T function) throws FunctionException;
}
