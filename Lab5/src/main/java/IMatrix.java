/**
 * Интерфейс IMatrix (квадратная матрица вещественных чисел), содержащий
 * методы:
 * - получить элемент с заданными индексами,
 * - изменить элемент с заданными индексами,
 * - вычислить определитель матрицы  методом Гаусса.
 */

public interface IMatrix {
    double getElem(int i, int j) throws MatrixException;
    void editElem(int i, int j, double elem) throws MatrixException;
    double getDeterminant() throws MatrixException;
}
