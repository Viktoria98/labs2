import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ListDemoTest {
    @Test
    public void testCountOfNamesakes() throws HumanException {
        Human Henry = new Human("Jhonson", "Henry", "Baker", 45);
        List<Human> list = new LinkedList<>();
        List<Human> res = new LinkedList<>();
        list.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));

        list.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        res.add(new Human("Jhonson", "Timmy", "Cloud", 25));

        list.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        res.add(new Human("Jhonson", "Boris", "Petrovich", 125));

        list.add(new Human("Jhonsons", "Boriz", "Zovich", 87));
        list.add(new Human("Rainbower", "Zins", "Chovich", 77));

        list.add(new Human("Jhonson", "Reobert", "Grober", 7));
        res.add(new Human("Jhonson", "Reobert", "Grober", 7));
        assertEquals(res, ListDemo.countOfNamesakes(list, Henry));
    }

    @Test
    public void testHumansWithBiggestAge() throws HumanException {
        List<Human> obj = new LinkedList<>();
        Set<Human> res = new HashSet<>();
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 42, "Ll"));

        obj.add(new Human("Kllda", "Adsds", "Qqadsa", 82));
        res.add(new Human("Kllda", "Adsds", "Qqadsa", 82));

        obj.add(new Human("Kll", "Ads", "Dsa", 12));

        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        res.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));

        obj.add(new Human("dsdKllda", "sAdsds", "Qqadsa", 52));

        assertEquals(res, ListDemo.humansWithBiggestAge(obj));
    }

}
