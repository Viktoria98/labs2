import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class PersonSerializer extends StdSerializer<Person> {
    public PersonSerializer(){
        super(Person.class);
    }

    @Override
    public void serialize(@NotNull Person person, @NotNull JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("fullname", person.getName() + " " + person.getLastName() + " "
                + person.getSurName());
        jsonGenerator.writeNumberField("day", person.getDay());
        jsonGenerator.writeNumberField("month", person.getMonth());
        jsonGenerator.writeNumberField("year", person.getYear());
        jsonGenerator.writeEndObject();
    }
}
