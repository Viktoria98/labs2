import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.*;

public class FileDemo {
    /**
     * 1.Метод записывает массив целых чисел в двоичный поток.
     */
    public static void WriteArrayInStream(@NotNull OutputStream out, @NotNull int []arr) throws IOException {
        try (DataOutputStream arrWrite = new DataOutputStream(out)) {
            for (int temp : arr) {
                arrWrite.writeInt(temp);
            }
        }
    }

    /**
     * 1.Метод читает массив целых чисел из двоичного потока.
     * Предполагается, что до чтения массив уже создан, нужно прочитать n
     * чисел, где n — длина массива.
     */
    @NotNull
    public static int[] ReadArrayFromStream(InputStream in) throws IOException{
        try (DataInputStream arrRead = new DataInputStream(in)){
            int [] arr = new int[arrRead.available()/4];
            for(int i = 0; i < arr.length; i++){
                arr[i] = arrRead.readInt();
            }
            return arr;
        }
    }

    /**
     * 2.Метод записывает массив целых чисел в символьный поток.
     * В потоке числа разделяются пробелами.
     */
    public static void WriteArrayInCharsStream(@NotNull Writer out, @NotNull int[]arr) throws IOException {
        try(BufferedWriter arrWrite = new BufferedWriter(out)) {
            for(int temp: arr){
                arrWrite.write(String.valueOf(temp));
                arrWrite.write(' ');
            }
        }
    }

    /**
     * 2.Метод читает массив целых чисел из символьного потока.
     * Предполагается, что до чтения массив уже создан, нужно прочитать n
     * чисел, где n — длина массива.
     * В потоке числа разделяются пробелами.
     */
    @NotNull
    public static int[] ReadArrayFromCharsStream(@NotNull Reader in) throws IOException {
        try(BufferedReader arrRead = new BufferedReader(in)) {
            String[] numbers = arrRead.readLine().split(" ");
            int [] res = new int[numbers.length];
            for(int i = 0; i < res.length; i++){
                res[i] = Integer.parseInt(numbers[i]);
            }
            return res;
        }
   }

    /**
     * 3.Метод используя класс RandomAccessFile, читает массив целых чисел, начиная с заданной
     * позиции.
     */
    @NotNull
    public static int[] ReadArrayFromStreamPosition(long position, File fileName) throws IOException {
        try(RandomAccessFile readStream = new RandomAccessFile(fileName, "r")) {
            try {
                readStream.seek(position);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String [] numbers = readStream.readLine().split(" ");
            int [] result = new int[numbers.length];
            for(int i = 0; i < result.length; i++){
                result[i] = Integer.parseInt(numbers[i]);
            }
            return result;
        }
    }

    /**
     * 4.Метод используя класс File, получает список всех файлов с заданным расширением в заданном
     * каталоге (поиск в подкаталогах выполнять не надо).
     */
    @NotNull
    public static List<File> FilesWithExpansionInCatalog(String expansion, @NotNull File dir) throws IOException {

        List<File> res = new ArrayList<>();
        File[] listFiles = dir.listFiles(File::isFile);

        if(listFiles == null) {
            throw new IOException("Empty catalog");
        }
        for (File temp: listFiles){
            if(temp.getName().endsWith(expansion)){ res.add(temp);}
        }
        return res;
    }

    /**
     * 5.Метод получает в заданном каталоге список всех файлов и подкаталогов,
     * имена которых удовлетворяют заданному регулярному выражению. Поиск распространяется
     * в подкаталоги. Имена найденных файлов должны быть вместе с
     * абсолютными путями.
     */
    @NotNull
    public static List<String> filesWithRegex(@NotNull File file, String regex) throws IOException {
        if(!file.isDirectory()){
            throw new IOException("It`s not directory");
        }
        List<String> res = new ArrayList<>();
        File[] files = file.listFiles();
        if(files != null) {
            for (File temp : files) {
                if(temp.getName().matches(regex)){
                    res.add(temp.getAbsolutePath());
                }
                if(temp.isDirectory()){
                    res.addAll(filesWithRegex(temp, regex));
                }
            }
        }
        return res;
    }

    /**
     * 7.Напишите метод сохранения объекта типа House в csv-файл (о формате csv). Используйте
     * символ ; как разделитель. Имя файла должно строится по шаблону
     * house_кадастровый_номер_дома.csv.
     */
    public static void writeHouseCsv(@NotNull House house) throws IOException {
        File file = new File("house"+house.getCadastralNumber()+".csv");
        Writer writer = new FileWriter(file);

        writer.write("Данные о доме\n");
        writer.write("Кадастровый номер:" + house.getCadastralNumber()+"\n");
        writer.write("Адрес:" + house.getAddress() + "\n");
        writer.write("Старший по дому:" + house.getHouseWife().getLastName() + " " + house.getHouseWife().getName());
        writer.write(" " + house.getHouseWife().getSurName() + "\n");
        writer.write("Данные о квартирах\n");
        writer.write("No;Площадь(кв. м);Владельцы\n");
        int number = 1;
        for(Flat temp:house.getApartments()){
            writer.write(number + ";"+temp.getArea()+";");
            int personCounter = 1;
            for(Person tempPerson:temp.getOwners()){
                writer.write(tempPerson.getLastName() + " " + tempPerson.getName().substring(0,1) + "." +
                        tempPerson.getSurName().substring(0,1) + ".");
                if(personCounter != temp.getOwners().size()){
                    writer.write(",");
                }
                personCounter++;
            }
            writer.write("\n");
            number++;
        }
        writer.close();
    }

}
