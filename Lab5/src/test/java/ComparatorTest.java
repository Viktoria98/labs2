import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComparatorTest {

    @Test
    void compare() throws MatrixException {
        //71
        Matrix test = new Matrix(new double[]{3., 2., 4., 1., 5., 6., 4., 1., 9.}, 3);
        //15
        Matrix test1 = new Matrix(new double[]{0.,5.,0.,1.,4.,2.,0.,1.,7.,4.,1.,1.,5.,0.,0.,0.}, 4);
        //252
        UpTriangleMatrix test2 = new UpTriangleMatrix(3, 3, 4, 5, 7, 9, 12);
        //60
        DiagonalMatrix test3 = new DiagonalMatrix(3, new double[]{1, 2, 30});
        Comparator<Matrix> testComp = new Comparator<>();
        assertEquals(56, testComp.compare(test, test1));
        assertEquals(192,testComp.compare(test2,test3));

    }
}