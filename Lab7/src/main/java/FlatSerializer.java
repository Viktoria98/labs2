import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class FlatSerializer extends StdSerializer<Flat> {
    public FlatSerializer() {
        super(Flat.class);
    }

    @Override
    public void serialize(@NotNull Flat flat, @NotNull JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("apartmentNumber", flat.getApartmentNumber());
        jsonGenerator.writeNumberField("area", flat.getArea());
        jsonGenerator.writeArrayFieldStart("owners");
        for(Person temp: flat.getOwners()){
            jsonGenerator.writeObject(temp);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
