/**
 * Ппроизводный класс (от класса Matrix) DiagMatrix (диагональная матрица). Размерность матрицы
 * определяется при ее создании и в дальнейшем не меняется. Содержит конструктор по
 * размерности матрицы и конструктор по набору элементов на диагонали. Метод изменения
 * элемента при попытке записать ненулевое значение вне диагонали выбрасывает
 * исключение.
 */
public class DiagonalMatrix extends Matrix {

    public DiagonalMatrix(int N,double[] diagonalsElems) throws MatrixException {
        super(N);
        if(diagonalsElems.length != N){
            throw new MatrixException("Несоответвущая длина массива диагональных элементов", diagonalsElems.length);
        }
        for(int i = 0; i < diagonalsElems.length; i++){
            editElem(i,i, diagonalsElems[i]);
        }
    }

    @Override
    public void editElem(int i, int j, double elem) throws MatrixException {
        if(i >= getMatrixSize() || i < 0){
            throw new MatrixException("Некоректный индекс массива", i);
        }
        else if(j >= getMatrixSize() || j < 0){
            throw new MatrixException("Некоректный индекс массива", j);
        }
        else if(i != j){
            throw new MatrixException("Некоректный индекс для диагонального матрицы");
        }
        super.editElem(i, j, elem);
    }

}
