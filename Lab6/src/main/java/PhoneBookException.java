public class PhoneBookException extends Exception {
    PhoneBookException(String error){
        super(error);
    }
}
