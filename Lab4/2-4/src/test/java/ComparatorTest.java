import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComparatorTest {

    @Test
    void compare() throws GoodsException {
        Goods test = new Goods("Car", "audi");
        Goods test1 = new Goods("Domino" , "1-2-3-4-5-6");
        Goods test2 = new Goods("Car", "cmw");
        Comparator<Goods> testComp = new Comparator<>();
        assertEquals(-1, testComp.compare(test, test1));
        assertEquals(-2 ,testComp.compare(test,test2));
    }
}