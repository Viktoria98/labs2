import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FlatDeserializer extends StdDeserializer<Flat> {
    public FlatDeserializer() {
        super(Flat.class);
    }

    @Override
    public Flat deserialize(@NotNull JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        int area = 0, apartmentNumber = 0;
        List<Person> arr = new ArrayList<>();
        while (jsonParser.nextToken() != JsonToken.END_OBJECT){
            String fieldname = jsonParser.getCurrentName();
            if("area".equals(fieldname)){
                jsonParser.nextToken();
                area = jsonParser.getIntValue();
            }
            else if("apartmentNumber".equals(fieldname)){
                jsonParser.nextToken();
                apartmentNumber = jsonParser.getIntValue();
            }
            if("owners".equals(fieldname)){
                jsonParser.nextToken();
                while(jsonParser.nextToken() != JsonToken.END_ARRAY){
                    arr.add(jsonParser.readValueAs(Person.class));
                }
            }
        }
        return new Flat(apartmentNumber, area, arr);
    }
}
