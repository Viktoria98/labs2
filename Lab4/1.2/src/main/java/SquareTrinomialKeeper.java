/**
 * Класс с полем типа «Квадратный трехчлен»
 */
public class SquareTrinomialKeeper {
    private SquareTrinomial squareTrinomial;

    public SquareTrinomialKeeper(double a, double b, double c){
        squareTrinomial = new SquareTrinomial(a, b, c);
    }

    /**
     * Метод, возвращает больший корень или выбрасывает исключение, если корней нет
     * @return Больший корень
     * @throws IllegalArgumentException Если корней нет
     */
    public double getGreaterRoot() throws IllegalArgumentException{
        double result;
        try {
            double[] roots = squareTrinomial.getRoots();
            result = Math.max(roots[0], roots[1]);
        }
        catch (IllegalArgumentException exception){
            throw new IllegalArgumentException("Корней нет");
        }
        return result;
    }
}
