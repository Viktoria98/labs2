import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IntegralFunctionalTest {

    @Test
    void getValue() throws FunctionException {
        IntegralFunctional test = new IntegralFunctional(0,-2);
        IntegralFunctional test1 = new IntegralFunctional(-2,2);
        IntegralFunctional test2 = new IntegralFunctional(0,7);
        LinearFunction func = new LinearFunction(2,4,-100,100);
        assertThrows(FunctionException.class, ()-> test.getValue(null));

        assertEquals(-4, test.getValue(func));
        assertEquals(16,test1.getValue(func));
        assertEquals(77,test2.getValue(func));
    }
}