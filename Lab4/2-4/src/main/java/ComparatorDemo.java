import java.util.Arrays;

/**
 * Класс ComparatorDemo со статическим методом sortGoods, который получает на
 * вход массив товаров и компаратор и сортирует массив по возрастанию
 */

public class ComparatorDemo {
    public static void sortGoods(Goods[] arr, Comparator<? super Goods> comparator){
        Arrays.sort(arr, comparator);
    }
}
