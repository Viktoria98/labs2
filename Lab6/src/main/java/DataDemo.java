import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Класс DataDemo со статическим методом getAll, который получает на вход
 * объект класса Data и возвращает список целых чисел. Метод с помощью итератора
 * строит список всех чисел, входящих во все группы данных.
 */
public class DataDemo {
    @NotNull
    @Contract("_ -> !null")
    public static List<Integer> getAll(@NotNull Data data) {
        List<Integer> res = new ArrayList<>();
        Iterator<Integer> it = data.iterator();
        for (; it.hasNext(); ) {
            res.add(it.next());
        }
        return res;
    }
}
