import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class PersonDeserializer extends StdDeserializer<Person> {

    public PersonDeserializer(){
        super(Person.class);
    }

    @Override
    public Person deserialize(@NotNull JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        String [] fio = new String[3];
        int day = 0, month = 0, year = 0;
        while(jsonParser.nextToken() != JsonToken.END_OBJECT){
            String fieldname = jsonParser.getCurrentName();
            if("fullname".equals(fieldname)){
                jsonParser.nextToken();
                fio = jsonParser.getText().split(" ");
            }
            else if("day".equals(fieldname)){
                jsonParser.nextToken();
                day = jsonParser.getIntValue();
            }
            else if("month".equals(fieldname)){
                jsonParser.nextToken();
                month = jsonParser.getIntValue();
            }
            else if("year".equals(fieldname)){
                jsonParser.nextToken();
                year = jsonParser.getIntValue();
            }
        }
        try {
            return new Person(fio[0], fio[1], fio[2], day, month, year);
        } catch (PersonException e) {
            e.printStackTrace();
        }
        throw new IOException("Incorrect parser");
    }
}
