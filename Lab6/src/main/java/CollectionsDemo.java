import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class CollectionsDemo {
    /**
     * 1.Вход: список строк и символ. Выход: количество строк входного списка, у которых первый
     * символ совпадает с заданным.
     */
    public static int countOfStrings(@NotNull List <String>  arr, char firstChar){
        int counter = 0;
        for(String s:arr){
            if((s != null && !s.isEmpty()) && s.charAt(0) == firstChar){counter++;}
        }
        return counter;
    }

    /**
     * 3.Вход: список объектов типа Human и еще один объект типа Human. Выход — копия
     * входного списка, не содержащая выделенного человека. При изменении элементов
     * входного списка элементы выходного изменяться не должны.
     */
    @NotNull
    public static List<Human> copyWithoutOneHuman(@NotNull List<Human> arr, Human human){
        List<Human> res = new LinkedList<>();
        for(Human temp: arr){
            if(!temp.equals(human)){res.add(temp);}
        }
        return res;
    }

    /**
     * 4.Вход: список множеств целых чисел и еще одно множество. Выход: список всех множеств
     * входного списка, которые не пересекаются с заданным множеством.
     */
    @NotNull
    public static List<Set<Integer>> DisjointSets(@NotNull List<Set<Integer>> arr, @NotNull Set<Integer> set){
        List<Set<Integer>> res = new ArrayList<>();
        for(Set<Integer> temp: arr){
            temp.retainAll(set);
            if(temp.size() == 0){
                res.add(temp);
            }

        }
        return res;
    }

    /**
     * 6.Метод по множеству объектов, расширяющих Human, строит список так, чтобы итератор
     * списка перебирал его элементы по возрастанию ФИО людей без дополнительной
     * сортировки списка.
     */
    @NotNull
    @Contract("_ -> new")
    public static List<Human> sortedHumansList(Set<Human> set){
        SortedSet<Human> result = new TreeSet<>(set);
        return new LinkedList<>(result);
    }

    /**
     * 7.Метод, получает на вход отображение (Map) целочисленных идентификаторов
     * в объекты типа Human и множество целых чисел.
     * Результат — множество людей, идентификаторы которых содержатся во входном
     * множестве.
     */
    @NotNull
    public static Set<Human> humansWithIdentifiersInSet(@NotNull Map<Integer, Human> arr, Set<Integer> set){
        Set<Human> res = new HashSet<>();
        Set<Integer> keys = arr.keySet();
        for(Integer temp: keys){
            if(set.contains(temp)){res.add(arr.get(temp));}
        }
        return res;
    }

    /**
     * 8.Метод строит список идентификаторов людей, чей возраст не менее 18 лет для отображения
     * (Map) целочисленных идентификаторов в объекты типа Human и множество целых чисел.)
     */
    @NotNull
    public static List<Integer> personAgedUnder18Years(@NotNull Map<Integer, Human> arr){
        List <Integer> res = new LinkedList<>();
        Set<Integer> keys = arr.keySet();
        for(Integer temp: keys){
            if(arr.get(temp).getAge() < 18){ res.add(temp);}
        }
        return res;
    }

    /**
     * 9.Метод строит новое отображение, которое идентификатору сопоставляет возраст человека.
     */
    @NotNull
    public static Map<Integer, Integer> mappingIdentifierToHumanAge(@NotNull Map<Integer, Human> arr){
        Map<Integer, Integer> res = new HashMap<>();
        Set<Integer> keys = arr.keySet();
        for(Integer temp: keys){
            res.put(temp, arr.get(temp).getAge());
        }
        return res;
    }

    /**
     * 10.Метод по множеству объектов типа Human строит отображение, которое целому числу
     * (возраст человека) сопоставляет список всех людей данного возраста из входного
     * множества.
     */
    @NotNull
    public static Map<Integer,List<Human>> mappingAgeToListOfPeople(@NotNull Set<Human> arr) {
        Map<Integer, List<Human>> res = new HashMap<>();
        int tempAge;
        for (Human temp : arr) {
            tempAge = temp.getAge();
            if (res.containsKey(tempAge)) {
                res.get(tempAge).add(temp);
            }
            else {
                List<Human> tempList = new LinkedList<>();
                tempList.add(temp);
                res.put(tempAge, tempList);
            }
        }
        return res;
    }

    /**
     * 11.Метод с использованием решения задачи 10 по множеству людей строит отображение,
     * которое возрасту сопоставляет новое отображение, которое букве алфавита сопоставляет
     * список всех людей из входного множества, имеющих данный возраст, и фамилия которых
     * начинается на эту букву.
     * Т.е. (возраст -> (буква -> [список людей данного возраста с фамилией на эту букву]).
     * Списки внутри отсортированы по ФИО по убыванию.
     */
    @NotNull
    @Contract(pure = true)
    public static Map<Integer, Map<Character, List<Human>>> difficultMapping(@NotNull Map<Integer,List<Human>> data){
        Map<Integer, Map<Character, List<Human>>> res = new HashMap<>();
        Set<Integer> keys = data.keySet();
        for(int temp: keys){
            Map<Character, Set<Human>> tempMap = new HashMap<>();
            for(Human tempHuman: data.get(temp)){
                char firstChar = tempHuman.getLastName().charAt(0);
                if(!tempMap.containsKey(firstChar)){
                    SortedSet<Human> tempSet = new TreeSet<>(Comparator.reverseOrder());
                    tempSet.add(tempHuman);
                    tempMap.put(firstChar, tempSet);
                }
                else {
                    tempMap.get(firstChar).add(tempHuman);
                }
            }
            Map<Character, List<Human>> tempList= new HashMap<>();
            for(char tempChar: tempMap.keySet()){
                tempList.put(tempChar, new ArrayList<>(tempMap.get(tempChar)));
            }
            res.put(temp, tempList);
        }
        return res;
    }

}
