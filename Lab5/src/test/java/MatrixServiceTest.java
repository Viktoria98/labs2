import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixServiceTest {

    @Test
    void arrangeMatrices() throws MatrixException {
        //71
        Matrix test = new Matrix(new double[]{3., 2., 4., 1., 5., 6., 4., 1., 9.}, 3);
        //15
        Matrix test1 = new Matrix(new double[]{0.,5.,0.,1.,4.,2.,0.,1.,7.,4.,1.,1.,5.,0.,0.,0.}, 4);
        //252
        UpTriangleMatrix test2 = new UpTriangleMatrix(3, 3, 4, 5, 7, 9, 12);
        //60
        DiagonalMatrix test3 = new DiagonalMatrix(3, new double[]{1, 2, 30});
        //100
        UpTriangleMatrix test4 = new UpTriangleMatrix(2, 10,1, 10);
        Matrix[] matrixArr = new Matrix[]{test,test1,test2,test3,test4};

        MatrixService.arrangeMatrices(matrixArr);
        assertAll(()-> assertEquals(15, matrixArr[0].getDeterminant()),
                ()-> assertEquals(60, matrixArr[1].getDeterminant()),
                ()-> assertEquals(71,matrixArr[2].getDeterminant()),
                () -> assertEquals(100, matrixArr[3].getDeterminant()),
                () -> assertEquals(252, matrixArr[4].getDeterminant()));
    }
}