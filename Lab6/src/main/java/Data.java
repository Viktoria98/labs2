import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

/**
 * Класс Data (набор групп). Класс должен содержать название набора (строка
 * символов) и сам набор в виде массива.
 */
public class Data implements Iterable<Integer>{
    private String nameOfSet;
    private Group[] set;
    public Data(String nameOfSet, Group ... elems){
        this.nameOfSet = nameOfSet;
        this.set = elems;
    }

    public String getNameOfSet() {
        return nameOfSet;
    }

    public void setNameOfSet(String nameOfSet) {
        this.nameOfSet = nameOfSet;
    }

    public Group[] getSet() {
        return set;
    }

    public Group getGroup(int index) {
        return set[index];
    }

    public void setSet(Group[] set) {
        this.set = set;
    }

    public int getLength(){
        return set.length;
    }

    @NotNull
    @Override
    public Iterator<Integer> iterator() {
        return new DataIterator(this);
    }
}
