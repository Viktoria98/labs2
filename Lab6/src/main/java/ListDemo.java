import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ListDemo {
    /**
     * 2.Метод получает на вход список объектов типа Human
     * и еще один объект типа Human. Результат — список однофамильцев заданного человека
     * среди людей из входного списка.
     */
    @NotNull
    public static List<Human> countOfNamesakes(@NotNull List<Human> arr, Human human){
        List<Human> result = new LinkedList<>();
        for(Human temp: arr){
            if(human.getLastName().equals(temp.getLastName())){result.add(temp);}
        }
        return result;
    }

    /**
     * 5.Метод получает на вход список, состоящий из
     * объектов типа Human и его производных классов. Результат — множество людей из
     * входного списка с максимальным возрастом.
     */
    @NotNull
    public static Set<Human> humansWithBiggestAge(@NotNull List<Human> arr){
        Set<Human> result = new HashSet<>();
        int maxAge = 0;
        for(Human temp: arr){
            if(temp.getAge() > maxAge){ maxAge = temp.getAge(); }
        }
        for (Human temp: arr){
            if(temp.getAge() == maxAge){result.add(temp);}
        }
        return result;
    }
}
