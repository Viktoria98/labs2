/**
 * Класс-компаратор Для класса «Товар» из предыдущего задания (про наследование).
 * Два товара сравниваются по названию, если названия равны, то далее — по
 * описанию.
 * @param <T>
 */


public class Comparator<T extends Goods> implements java.util.Comparator<Goods> {
    @Override
    public int compare(Goods goods1, Goods goods2) {
        int result = goods1.getName().compareTo(goods2.getName());
        if (result == 0) {
            return goods1.getDescription().compareTo(goods2.getDescription());
        }
        return result;
    }
}
