import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HouseDeserialize extends StdDeserializer<House> {

    public HouseDeserialize(){
        super(House.class);
    }

    @Override
    public House deserialize(@NotNull JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        String cadastralNumber = "", address = "";
        Person p = new Person();
        List<Flat> list = new ArrayList<>();
        while (jsonParser.nextToken() != JsonToken.END_OBJECT){
            String fieldName = jsonParser.getCurrentName();
            if("cadastralNumber".equals(fieldName)){
                jsonParser.nextToken();
                cadastralNumber = jsonParser.getText();
            }
            else if("address".equals(fieldName)){
                jsonParser.nextToken();
                address = jsonParser.getText();
            }
            else if("housewife".equals(fieldName)){
                jsonParser.nextToken();
                p = jsonParser.readValueAs(Person.class);
            }
            else if("apartments".equals(fieldName)){
                jsonParser.nextToken();
                while(jsonParser.nextToken() != JsonToken.END_ARRAY){
                    list.add(jsonParser.readValueAs(Flat.class));
                }
            }
        }
        return new House(cadastralNumber, address, p, list);
    }
}
