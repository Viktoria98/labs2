import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinearFunctionTest {

    @Test
    void Constructor(){
        assertThrows(FunctionException.class, ()->new LinearFunction(12, 4, 10, 1));
        assertThrows(FunctionException.class, ()->new LinearFunction(0, 4, 10, 12));
    }

    @Test
    void getValue() throws FunctionException {
        LinearFunction test = new LinearFunction(1,2 , 3 ,5);
        assertThrows(FunctionException.class, ()-> test.getValue(6));
        assertEquals(6, test.getValue(4));

    }
}