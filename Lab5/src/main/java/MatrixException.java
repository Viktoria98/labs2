/**
 * Класс исключений для классов, использующих класс Matrix
 */

public class MatrixException extends Exception {
    int number;

    MatrixException(String message){
        super(message);
    }

    MatrixException(String message, int num){
        super(message);
        number = num;
    }
    public int getNumber(){
        return number;
    }
}
