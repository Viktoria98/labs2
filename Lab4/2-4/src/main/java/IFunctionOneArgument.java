/**
 * Интерфейс для понятия «функция одного вещественного аргумента,
 * определнная на отрезке [a; b]». Интерфейс содержит метод вычисления значения
 * функции при заданном аргументе и методы получения границ отрезка.
 */
public interface IFunctionOneArgument {
    double getStart();
    double getEnd();
    double getValue(double x) throws FunctionException;
}