import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PhoneBookTest {
    @Test
    public void testAddDeletePhone() throws HumanException, PhoneBookException {
        Map<Human, List<String>> map = new HashMap<>();
        Human h1 = new Human("Лиза", "Иванова", "Александрова",23);
        Human h2 = new Human("Александр", "Лукин", "Петрович",17);

        Map<Human, List<String>> res = new HashMap<>();

        List<String> phones1 = new ArrayList<String>();
        phones1.add("+79124328723");
        phones1.add("+79194623903");

        List<String> phones2 = new ArrayList<String>();
        phones2.add("+79511123903");

        res.put(h1, phones1);
        res.put(h2, phones2);

        PhoneBook phoneBook = new PhoneBook();
        phoneBook.addPhone(h1, "+79124328723");
        phoneBook.addPhone(h1, "+79194623903");
        phoneBook.addPhone(h2, "+79511123903");

        assertEquals(res, phoneBook.getContacts());

        Map<Human, List<String>> res1 = new HashMap<>();
        res1.put(h2, phones2);
        List<String> phones3 = new ArrayList<String>();
        phones3.add("+79124328723");
        res1.put(h1, phones3);

        phoneBook.deletePhone(h1, "+79194623903");

        assertEquals(res1, phoneBook.getContacts());
    }

    @Test
    public void testGetPhones() throws HumanException, PhoneBookException {
        Human h1 = new Human("Лиза", "Иванова", "Александрова",23);
        Human h2 = new Human("Александр", "Лукин", "Петрович",17);

        List<String> res = new ArrayList<String>();
        res.add("+79124328723");
        res.add("+79194623903");

        PhoneBook phoneBook = new PhoneBook();
        phoneBook.addPhone(h1, "+79124328723");
        phoneBook.addPhone(h1, "+79194623903");
        phoneBook.addPhone(h2, "+79511123903");

        assertEquals(res, phoneBook.getPhones(h1));
    }

    @Test
    public void testSearchHuman() throws HumanException, PhoneBookException {
        Human h1 = new Human("Лиза", "Иванова", "Александрова",23);
        Human h2 = new Human("Александр", "Лукин", "Петрович",17);

        final PhoneBook phoneBook = new PhoneBook();
        phoneBook.addPhone(h1, "+79124328723");
        phoneBook.addPhone(h1, "+79194623903");
        phoneBook.addPhone(h2, "+79511123903");

        assertEquals(h1, phoneBook.searchHuman("+79124328723"));
        assertThrows(PhoneBookException.class, ()->phoneBook.searchHuman("+71234567891"));

    }

    @Test
    public void testGetContactsPeopleWithSameBeginLastname() throws HumanException, PhoneBookException {
        Map<Human, List<String>> res = new HashMap<>();
        Human h1 = new Human("Елизавета", "Иванова", "Александрова",23);
        Human h2 = new Human("Александр", "Лукин", "Петрович",17);
        Human h3 = new Human("Авакий", "Лузин", "Викторович", 32);
        Human h4 = new Human("Андрей", "Тараканский", "Федорович", 22);

        String p1 = "+79124328723";
        String p2 = "+79149318723";
        String p3 = "+79867441712";
        String p4 = "+79570429542";
        String p5 = "+79975189077";
        String p6 = "+79197479012";

        PhoneBook phoneBook = new PhoneBook();
        phoneBook.addPhone(h1, p1);
        phoneBook.addPhone(h1, p2);
        phoneBook.addPhone(h2, p3);
        phoneBook.addPhone(h3, p4);
        phoneBook.addPhone(h3, p5);
        phoneBook.addPhone(h4, p6);

        res.put(h2, phoneBook.getPhones(h2));
        res.put(h3, phoneBook.getPhones(h3));

        assertEquals(res, phoneBook.getContactsPeopleWithSameBeginLastname("Лу"));

    }
}
