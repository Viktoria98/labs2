import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Класс LambdaDemo с набором открытых статических неизменяемых полей,
 * которым в качестве значений присвоены следующие лямбда-выражения:
 */
public class LambdaDemo {
    /**
     * 3.1. По строке символов получаем ее длину
     */
    public static final Function<String, Integer> strLen = str ->
            Optional.ofNullable(str).map(String::length).orElse(null);

    /**
     * 3.2. Для строки символов получаем ее первый символ, если он существует, или null иначе
     */
    public static final Function<String, Character> firstChar = str ->
            Optional.ofNullable(str).filter(z -> !z.isEmpty()).map(w->w.charAt(0)).orElse(null);
        //if(str == null || str.isEmpty()){return null;}
        //return str.charAt(0);
    ;

    /**
     * 3.3. Для строки проверяем, что она не содержит пробелов
     */
    public static final Function<String, Boolean> haventWhitespace = str ->{
        if(str == null || str.isEmpty()){return null;}
        return !str.contains(" ");
    };

    /**
     * 3.4. По строке получаем количество слов в ней(слова в строке разделены запятыми)
     */
    public static final Function<String, Integer> countOfWords = str ->{
        if (str == null || str.isEmpty()){return null;}
        int res = 0;
        boolean emptyOrNotWord = true;
        for(char temp: str.toCharArray()){
            if(Character.isLetter(temp)){
                emptyOrNotWord = false;
            }
            else if(temp == ',' && !emptyOrNotWord){res++;}
            else{emptyOrNotWord = true;}
        }
        if(!emptyOrNotWord){
            res++;
        }
        return res;
    };

    /**
     * 3.5. По человеку получаем его возраст
     */

    public static final Function<Human, Integer> ageOfHuman = Human::getAge;

    /**
     * 3.6. По двум людям проверяем, что у них одинаковая фамилия
     */
    public static final BiFunction<Human, Human, Boolean> haveSameLastname = (people1, people2) ->
            people1.getLastName().equals(people2.getLastName());

    /**
     * 3.7.Получаем фамилию, имя и отчество человека в виде одной строки(разделитель — пробел)
     */
    public static final Function<Human, String> getInitials =
            x -> x.getName() + " " + x.getLastName() + " " + x.getSurname();

    /**
     * 3.8. Делает человека старше на один год(по объекту Human создается новый объект)
     */
    public static final Function<Human, Human> personPlusPlus = human -> {
        try {
            return new Human(human.getLastName(), human.getName(), human.getSurname(), human.getAge()+1,
                    human.getGender());
        } catch (HumanException e) {
            e.printStackTrace();
        }
        return null;
    };

    /**
     * 3.9. По трем людям и заданному возрасту maxAge проверяет, что все три человека моложе maxAge.
     */
    public static final TripleToOne checkAge = (people, people1, people2, maxAge) ->
            people.getAge() < maxAge && people1.getAge() < maxAge && people2.getAge() < maxAge;

}
