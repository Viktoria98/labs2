import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComparatorDemoTest {

    @Test
    void sortGoods() throws GoodsException {
        Goods test = new Goods("drs", "bg");
        WeightGoods test1 = new WeightGoods("vba", "ca", 2);
        PieceGoods test2 = new PieceGoods("abc", "bt",5);
        Goods test3 = new Goods("ci", "dtz");
        PieceGoods test4 = new PieceGoods("vba", "ars", 3);
        Goods[] arr = {test, test1, test2, test3, test4};
        Comparator<Goods> comp = new Comparator<>();
        ComparatorDemo.sortGoods(arr, comp);
        assertAll(()-> assertEquals("abc", arr[0].getName()),
                ()->assertEquals("ci", arr[1].getName()),
                ()->assertEquals("drs",arr[2].getName()),
                ()->assertEquals("ars",arr[3].getDescription()),
                ()->assertEquals("ca",arr[4].getDescription()));
    }
}