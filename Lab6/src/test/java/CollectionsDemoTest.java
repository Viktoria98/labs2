import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CollectionsDemoTest {
    @Test
    public void testCountOfStrings(){
        List<String> obj = new LinkedList<>();
        obj.add("ADs");
        obj.add("SsssaSSSS");//1
        obj.add("sAczxccxgdv");
        obj.add("Scxzxsqffqf");//2
        obj.add("SSczxczxs");//3
        obj.add("SsSsSsSs");//4
        obj.add(" S");
        obj.add("");
        obj.add("_S1dsa");
        obj.add("Sdsadasd");//5
        assertEquals(5, CollectionsDemo.countOfStrings(obj, 'S'));
    }

    @Test
    public void testCopyWithoutOneHuman() throws HumanException {
        List<Human> list = new LinkedList<>();
        List<Human> res = new LinkedList<>();
        Human Henry = new Human("Jhonson", "Henry", "Baker", 45);
        list.add(Henry);

        list.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));
        res.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));
        list.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        res.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        list.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        res.add(new Human("Jhonson", "Boris", "Petrovich", 125));

        assertEquals(res, CollectionsDemo.copyWithoutOneHuman(list, Henry));

    }

    @Test
    public void testDisjointSets(){
        Set<Integer> mainSet = new HashSet<>();
        mainSet.add(1);
        mainSet.add(2);
        mainSet.add(3);

        Set<Integer> addSet1 = new HashSet<>();
        Set<Integer> addSet2 = new HashSet<>();//+
        Set<Integer> addSet3 = new HashSet<>();//+
        Set<Integer> addSet4 = new HashSet<>();

        addSet1.add(4);
        addSet1.add(3);

        addSet2.add(5);
        addSet2.add(9);
        addSet2.add(0);

        addSet3.add(12);
        addSet3.add(122);

        addSet4.add(2);
        addSet4.add(7);

        List<Set<Integer>> list = new ArrayList<>();
        list.add(addSet1);
        list.add(addSet2);
        list.add(addSet3);
        list.add(addSet4);

        List<Set<Integer>> res = new ArrayList<>();
        res.add(addSet2);
        res.add(addSet3);

        assertEquals(res, CollectionsDemo.DisjointSets(list, mainSet));

    }

    @Test
    public void testSortedHumansList() throws HumanException {
        Set<Human> set = new HashSet<>();
        Human human1 = new Human("dada", "dsad", "dsadas", 23);
        Human human2 = new Human("adasda", "dasd", "dasz", 16);
        Human human3 = new Human("bdasd", "dsad", "sadas", 11);

        set.add(human1);
        set.add(human2);
        set.add(human3);

        List<Human> res = new LinkedList<>();
        res.add(human2);
        res.add(human3);
        res.add(human1);

        List<Human> methodRes = CollectionsDemo.sortedHumansList(set);
        assertEquals(res, methodRes);

    }

    @Test
    public void testHumansWithIdentifiersInSet() throws HumanException {
        Map<Integer, Human> map = new HashMap<>();
        Set<Integer> identifiers = new HashSet<>();
        Set<Human> res = new HashSet<>();

        Human Henry = new Human("Jhonson", "Henry", "Baker", 45);
        map.put(12, Henry);
        identifiers.add(12);
        res.add(Henry);

        Human Ivanov = new Human("Ivanov", "Ivan", "Ivanovich", 66);
        map.put(56, Ivanov);

        Human Jhonson = new Human("Jhonson", "Timmy", "Cloud", 25);
        map.put(19, Jhonson);
        identifiers.add(19);
        res.add(Jhonson);

        Human Boris = new  Human("Jhonson", "Boris", "Petrovich", 125);
        map.put(55, Boris);

        assertEquals(res, CollectionsDemo.humansWithIdentifiersInSet(map, identifiers));


    }

    @Test
    public void testPersonAgedUnder18Years() throws HumanException {
        Map<Integer, Human> map = new HashMap<>();
        List<Integer> res = new ArrayList<>();

        Human Henry = new Human("Jhonson", "Henry", "Baker", 45);
        map.put(12, Henry);

        //+
        Human Ivanov = new Human("Ivanov", "Ivan", "Ivanovich", 13);
        map.put(56, Ivanov);

        //+
        Human Jhonson = new Human("Jhonson", "Timmy", "Cloud", 17);
        map.put(19, Jhonson);

        Human Boris = new  Human("Jhonson", "Boris", "Petrovich", 125);
        map.put(55, Boris);

        res.add(19);
        res.add(56);

        assertEquals(res, CollectionsDemo.personAgedUnder18Years(map));
    }

    @Test
    public void testMappingIdentifierToHumanAge() throws HumanException {
        Map<Integer, Human> map = new HashMap<>();
        Map<Integer, Integer> res = new HashMap<>();

        Human Henry = new Human("Jhonson", "Henry", "Baker", 45);
        map.put(12, Henry);
        res.put(12,45);

        Human Ivanov = new Human("Ivanov", "Ivan", "Ivanovich", 13);
        map.put(56, Ivanov);
        res.put(56,13);

        Human Jhonson = new Human("Jhonson", "Timmy", "Cloud", 17);
        map.put(19, Jhonson);
        res.put(19, 17);

        Human Boris = new  Human("Jhonson", "Boris", "Petrovich", 125);
        map.put(55, Boris);
        res.put(55, 125);

        assertEquals(res, CollectionsDemo.mappingIdentifierToHumanAge(map));
    }

    @Test
    public void testMappingAgeToListOfPeople() throws HumanException {
        Set<Human> set = new HashSet<>();
        Map <Integer,List<Human>> res = new HashMap<>();

        Human Carl = new Human("Carl", "Jhonson", "Baker", 12);
        Human Ivanov = new Human("Ivanov", "Ivan", "Ivanovich", 12);
        set.add(Ivanov);
        set.add(Carl);
        LinkedList<Human> temp = new LinkedList<>();
        temp.add(Ivanov);
        temp.add(Carl);
        res.put(12, temp);

        Human Henry = new Human("Henry", "Jhonson", "Baker", 45);
        Human Boris = new  Human("Jhonson", "Boris", "Petrovich", 45);
        set.add(Boris);
        set.add(Henry);
        LinkedList<Human> temp1 = new LinkedList<>();
        temp1.add(Henry);
        temp1.add(Boris);
        res.put(45, temp1);

        Human Jhonson = new Human("Jhonson", "Timmy", "Cloud", 17);
        set.add(Jhonson);
        LinkedList<Human> temp2 = new LinkedList<>();
        temp2.add(Jhonson);
        res.put(17, temp2);

        assertEquals(res, CollectionsDemo.mappingAgeToListOfPeople(set));

    }

    @Test
    public void testDifficultMapping() throws HumanException {
        Map<Integer, Map<Character, List<Human>>> res = new HashMap<>();
        Set<Human> set = new HashSet<>();

        HashMap<Character, List<Human>> m1 = new HashMap<>();
        Human h1 = new Human("a", "Slade", "x", 23);
        Human h2 = new Human("b", "Barney", "y", 23);
        Human h3 = new Human("c", "Black", "w", 23);

        ArrayList<Human> p1 = new ArrayList<>();
        p1.add(h1);
        ArrayList<Human> p2 = new ArrayList<>();
        p2.add(h3);
        p2.add(h2);

        m1.put('S', p1);
        m1.put('B', p2);

        HashMap<Character, List<Human>> m2 = new HashMap<>();
        Human h4 = new Human("d", "Calabro", "z", 44);
        Human h5 = new Human("e", "Sudo", "i", 44);
        Human h6 = new Human("f", "Cudd", "k", 44);
        Human h7 = new Human("g", "Charon", "m", 44);

        ArrayList<Human> p3 = new ArrayList<>();
        p3.add(h6);
        p3.add(h7);
        p3.add(h4);
        ArrayList<Human> p4 = new ArrayList<>();
        p4.add(h5);
        m2.put('C', p3);
        m2.put('S', p4);

        res.put(23, m1);
        res.put(44, m2);

        set.add(h1);
        set.add(h2);
        set.add(h3);
        set.add(h4);
        set.add(h5);
        set.add(h6);
        set.add(h7);

        //System.out.println(CollectionsDemo.mappingAgeToListOfPeople(set));
        //System.out.println(res);

        assertEquals(res, CollectionsDemo.difficultMapping(CollectionsDemo.mappingAgeToListOfPeople(set)));
    }

}
