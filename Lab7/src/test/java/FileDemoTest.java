import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class FileDemoTest {

    @Test
    public void testWriteReadArrayBinStream() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(12);
        int[] arr = {3,4,5};
        FileDemo.WriteArrayInStream(byteArrayOutputStream, arr);
        byte [] bytes = byteArrayOutputStream.toByteArray();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        int [] actRes = FileDemo.ReadArrayFromStream(byteArrayInputStream);
        assertArrayEquals(arr, actRes);
    }

    @Test
    public void testWriteAndReadArrayCharsStream() throws IOException {
        CharArrayWriter in = new CharArrayWriter(16);
        int [] arr = new int[]{1,2,3};
        FileDemo.WriteArrayInCharsStream(in, arr);
        CharArrayReader out = new CharArrayReader(in.toCharArray());
        int [] res = FileDemo.ReadArrayFromCharsStream(out);
        assertArrayEquals(arr, res);
    }

    @Test
    public void testReadArrayFromStreamPosition(@TempDir File tempFile) throws IOException {
        File f1 = new File(tempFile, "test.txt");
        f1.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(f1));
        writer.write("check arr: 1 2 44 532");
        writer.close();
        long position = 11;
        int [] res = new int[]{1, 2, 44, 532};
        assertArrayEquals(res, FileDemo.ReadArrayFromStreamPosition(position, f1));
    }

    @Test
    public void testReadArrayFromStreamPositionException(@TempDir File tempFile) throws IOException {
        File f1 = new File(tempFile, "test.txt");
        f1.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(f1));
        writer.write("check arr: 1 2 44 532");
        writer.close();
        String fileName = "excTest.txt";
        String fileName1 = "excTest1.txt";
        long position = 40;

        assertThrows(NullPointerException.class, ()->FileDemo.ReadArrayFromStreamPosition(position, f1));
    }

    @Test
    public void testFilesWithExpansionInCatalog(@TempDir File anotherTempDir)  throws IOException {

        File t1 = new File(anotherTempDir, "test1.txt");
        t1.createNewFile();
        File t2 = new File(anotherTempDir, "rsa.bin");
        t2.createNewFile();
        File t3 = new File(anotherTempDir, "lbs.txt");
        t3.createNewFile();
        List<File> fileRes = FileDemo.FilesWithExpansionInCatalog("txt", anotherTempDir);

        List<String> res = new ArrayList<>();
        for(File temp: fileRes){
            res.add(temp.getName());
        }

        List<String> actRes = new ArrayList<>();
        actRes.add("test1.txt");
        actRes.add("lbs.txt");

        assertIterableEquals(actRes, res);
    }

    @Test
    public void testFilesWithRegex(@TempDir File anotherTempDir) throws IOException {

        File file1 = new File(anotherTempDir, "test12.bin");
        file1.createNewFile();

        File file2 = new File(anotherTempDir, "lsf.txt");
        file2.createNewFile();

        File folder = new File(anotherTempDir, "folder");
        folder.mkdir();

        File file3 = new File(folder, "test16.txt");
        file3.createNewFile();

        File folder1 = new File(anotherTempDir, "test11");
        folder1.mkdir();

        File folder2 = new File(anotherTempDir, "tests11");
        folder2.mkdir();

        List<String> res = FileDemo.filesWithRegex(anotherTempDir, "test1(.*)");
        res = res.stream().map((s)->s.substring(s.indexOf("test"))).collect(Collectors.toList());

        List<String> actRes = new ArrayList<>(Arrays.asList("test11", "test16.txt", "test12.bin"));

        assertIterableEquals(actRes, res);

    }

    /*Файл, получившийся в результате работы тестового метода как проверка*/
    @Test
    public void testWriteHouseCsv() throws PersonException, IOException {
        List<Flat> flats = new LinkedList<>();
        List<Person> personFlat2 = new LinkedList<>();
        List<Person> personFlat3 = new LinkedList<>();
        personFlat2.add(new Person("Олег", "Власов", "Юрьевич", 13, 2, 2002));
        personFlat2.add(new Person("Сергей", "Власов", "Денисович", 17, 8, 1980));

        personFlat3.add(new Person("Евгений", "Жданов", "Павловна", 25, 3, 1990));
        personFlat3.add(new Person("София", "Жданова", "Петровна", 19, 1, 2003));
        personFlat3.add(new Person("Елизавета", "Жданова", "Павловна", 7, 9, 1981));

        flats.add(new Flat(1, 110, Collections.singletonList(new Person(
                        "Иван", "Орлов", "Алексеевич", 1, 5, 1999))));
        flats.add(new Flat(2, 55, Collections.singletonList(new Person(
                "Александр", "Давыдов", "Сергеевич", 4, 1, 1977))));

        flats.add(new Flat(5, 23, personFlat2));
        flats.add(new Flat(11, 80, personFlat3));


        House house = new House("31", "ул. Мира 32", new Person
                ("Сергей", "Власов", "Денисович", 17, 8, 1980), flats);
        FileDemo.writeHouseCsv(house);
    }

}
