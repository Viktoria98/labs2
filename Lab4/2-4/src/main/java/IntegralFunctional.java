/**
 * Класс, реализующий интерфейс функционала с методом,
 * вычисляющим определенный интеграл на отрезке [a; b] (пределы интегрирования хранятся как поля и
 * устанавливаются конструктором, если область определения функции не содержится в
 * [a; b], то выбрасывается исключение),
 * Интегрирование производится методом прямоугольников.
 */

public class IntegralFunctional implements IFunctional{
    private double a,b;

    public IntegralFunctional(double a, double b){
        this.a = a;
        this.b = b;
    }

    @Override
    public double getValue(IFunctionOneArgument function) throws FunctionException {
        if(function == null){
            throw new FunctionException("Передан null");
        }
        else if(function. getStart() > a || b > function.getEnd()){
            throw new FunctionException("Функция неопределена на [a,b]");
        }
        if(a < 0 && b > 0){
            return (function.getValue(a/2)*(-a)) + (function.getValue(b/2) * b);
        }
        return function.getValue(a+b/2)*(b-a);
    }
}
