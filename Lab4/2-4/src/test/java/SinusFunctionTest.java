import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SinusFunctionTest {

    @Test
    void Constructor(){
        assertThrows(FunctionException.class, ()->new LinearFunction(12, 4, 10, 1));
        assertThrows(FunctionException.class, ()->new LinearFunction(0, 4, 10, 12));
    }

    @Test
    void getValue() throws FunctionException {
        double pi = 3.14159265;
        double epsilon = 10e-7;
        SinusFunction test = new SinusFunction(1, 2, 0, 5);
        assertThrows(FunctionException.class, ()->test.getValue(7));
        assertTrue(Math.abs(test.getValue(pi)) < epsilon);
        assertTrue(Math.abs(test.getValue(pi/4) -1 ) < epsilon);
    }
}