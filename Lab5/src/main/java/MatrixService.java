import java.util.Arrays;

/**
 * Класс MatrixService со статическим методом arrangeMatrices, который
 * получает на вход массив матриц произвольного вида и сортирует этот массив по неубыванию
 * определителей матриц (найдите в классе Arrays подходящий метод сортировки).
 */

public class MatrixService {
    public static void arrangeMatrices(Matrix ... arr) throws MatrixException {
        if(arr.length == 0){
            throw new MatrixException("Пустой массив матриц");
        }
        for(Matrix temp: arr){
            if(temp == null){
                throw new MatrixException("Пустой элемент массива матриц");
            }
        }

        Arrays.sort(arr, new Comparator<>());

    }
}
