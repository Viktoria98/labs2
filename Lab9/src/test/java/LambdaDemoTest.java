import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LambdaDemoTest {

    String s1 = "1";
    String s2 = "dazxc";
    String s3 = null;
    String s4 = "";
    String s5 = "das das";
    String s6 = "da,da,da";
    String s7 = "kf,gf,hy,ji";

    @Test
    void testStrLen(){
        assertEquals(1, LambdaDemo.strLen.apply(s1));
        assertEquals(5, LambdaDemo.strLen.apply(s2));
        assertNull(LambdaDemo.strLen.apply(s3));
        assertEquals(0, LambdaDemo.strLen.apply(s4));

    }

    @Test
    void testFirstChar(){
        assertEquals('1', LambdaDemo.firstChar.apply(s1));
        assertEquals('d', LambdaDemo.firstChar.apply(s2));
        assertNull(LambdaDemo.firstChar.apply(s3));
        assertNull(LambdaDemo.firstChar.apply(s4));
    }

    @Test
    void testHaventWhitespace(){
        assertTrue(LambdaDemo.haventWhitespace.apply(s1));
        assertTrue(LambdaDemo.haventWhitespace.apply(s2));
        assertNull(LambdaDemo.haventWhitespace.apply(s3));
        assertNull(LambdaDemo.haventWhitespace.apply(s4));
        assertFalse(LambdaDemo.haventWhitespace.apply(s5));
    }

    @Test
    void testCountOfWords(){
        assertEquals(0, LambdaDemo.countOfWords.apply(s1));
        assertEquals(1, LambdaDemo.countOfWords.apply(s2));
        assertNull(LambdaDemo.haventWhitespace.apply(s3));
        assertNull(LambdaDemo.haventWhitespace.apply(s4));
        assertEquals(3, LambdaDemo.countOfWords.apply(s6));
        assertEquals(4, LambdaDemo.countOfWords.apply(s7));
    }

    @Test
    public void testAgeOfHuman()  throws HumanException {
        Human h1 = new Human("dad", "czxc", "cx", 45, Human.Gender.M);
        Student h2 = new Student("das", "fas", "fas", 32, Human.Gender.M,
                "da", "dasdas", "da");

        assertEquals(45, LambdaDemo.ageOfHuman.apply(h1));
        assertEquals(32, LambdaDemo.ageOfHuman.apply(h2));
    }

    @Test
    void testHaveSameLastname() throws HumanException {
        Human h1 = new Human("das", "czxc", "cx", 45, Human.Gender.M);
        Student h2 = new Student("das", "fas", "fas", 32, Human.Gender.M,
                "da", "dasdas", "da");
        Human h3 = new Human("dxv", "vxcvxc", "fsdf", 19, Human.Gender.M);

        assertTrue(LambdaDemo.haveSameLastname.apply(h1, h2));
        assertFalse(LambdaDemo.haveSameLastname.apply(h2, h3));
    }

    @Test
    void testGetInitials() throws HumanException {
        Human h1 = new Human("Ivanov", "Ivan", "Ivanovich", 45, Human.Gender.M);
        Student h2 = new Student("Smith", "John", "Mapother", 32, Human.Gender.M,
                "da", "dasdas", "da");
        Human h3 = new Human("Gordon", "Adam", "Robinson", 19, Human.Gender.M);

        assertEquals("Ivan Ivanov Ivanovich", LambdaDemo.getInitials.apply(h1));
        assertEquals("John Smith Mapother", LambdaDemo.getInitials.apply(h2));
        assertEquals("Adam Gordon Robinson", LambdaDemo.getInitials.apply(h3));
    }

    @Test
    void testPersonPlusPlus() throws HumanException {
        Human h1 = new Human("Ivanov", "Ivan", "Ivanovich", 45, Human.Gender.M);
        Human h2 = new Human("Smith", "John", "Mapother", 32, Human.Gender.M);

        assertEquals(46, LambdaDemo.personPlusPlus.apply(h1).getAge());
        assertEquals(33, LambdaDemo.personPlusPlus.apply(h2).getAge());
    }

    @Test
    void testCheckAge() throws HumanException {
        Human h1 = new Human("das", "czxc", "cx", 17, Human.Gender.M);
        Human h2 = new Human("das", "fas", "fas", 29, Human.Gender.F);
        Human h3 = new Human("dxv", "vxcvxc", "fsdf", 25, Human.Gender.M);
        Human h4 = new Human("dxv", "vxcvxc", "fsdf", 26, Human.Gender.F);

        int maxAge = 30;
        int maxAge1 = 28;

        assertTrue(LambdaDemo.checkAge.check(h1,h2,h3, maxAge));
        assertFalse(LambdaDemo.checkAge.check(h2,h3,h4, maxAge1));
    }


}
