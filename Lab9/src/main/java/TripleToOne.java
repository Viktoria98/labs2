public interface TripleToOne {
    boolean check(Human human, Human human1, Human human2, int maxAge);
}
