import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    double[] arr = {0.,5.,0.,1.,4.,2.,0.,1.,7.,4.,1.,1.,5.,0.,0.,0.};
    double[] arr1 = {0.,0.,0.,2.,9.,7.,2.,4.,0.,0.,3.,9.,1.,2.,7.,3.};
    double[] arr2 = {1.,2.,3.,4.,5.,6.,7.,8.,9.};
    double[] arr3 = {3.,2.,4.,1.,5.,6.,4.,1.,9.};

    int matrixSize = 4;
    int matrixSize1 = 3;
    Matrix matrix = new Matrix(arr, matrixSize);
    Matrix matrix1 = new Matrix(arr1, matrixSize);
    Matrix matrix2 = new Matrix(arr2, matrixSize1);
    Matrix matrix3 = new Matrix(arr3, matrixSize1);

    MatrixTest() throws MatrixException {
    }

    @Test
    void getElem() throws MatrixException{
        assertThrows(MatrixException.class, ()-> matrix.getElem(4,6));
        assertEquals(0,matrix.getElem(1,2));
        assertEquals(7,matrix2.getElem(2,0));
    }

    @Test
    void getDeterminant() throws MatrixException {
//https://www.wolframalpha.com/input/?i=%7B%280%2C5%2C0%2C1%29%2C%284%2C2%2C0%2C1%29%2C%287%2C4%2C1%2C1%29%2C%285%2C0%2C0%2C0%29%7D
        assertEquals(15, matrix.getDeterminant());
//https://www.wolframalpha.com/input/?i=%7B%280%2C0%2C0%2C2%29%2C%289%2C7%2C2%2C4%29%2C%280%2C0%2C3%2C9%29%2C%281%2C2%2C7%2C3%29%7D
        assertEquals(66, matrix1.getDeterminant());
//https://www.wolframalpha.com/input/?i=%7B%281%2C2%2C3%29%2C%284%2C5%2C6%29%2C%287%2C8%2C9%29%7D
        assertEquals(0,matrix2.getDeterminant());
//https://www.wolframalpha.com/input/?i=%7B%283%2C2%2C4%29%2C%281%2C5%2C6%29%2C%284%2C1%2C9%29%7D&lang=ru
        assertEquals(71,matrix3.getDeterminant());
    }
}