/**
 * Класс, реализующий интерфейс функционала
 * C методом. находящим сумму значений функции на концах отрезка и в его середине.
 */

public class SumFunctional implements IFunctional{
    @Override
    public double getValue(IFunctionOneArgument function) throws FunctionException {
        if(function == null){
            throw new FunctionException("Передан null");
        }
        return function.getValue(function.getStart()) + function.getValue(function.getEnd()) +
                function.getValue((function.getEnd() + function.getStart())/2);
    }
}
