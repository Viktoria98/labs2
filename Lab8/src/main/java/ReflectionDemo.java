import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {
    /**
     * 1.Дан список объектов произвольных типов. Метод находит количество элементов списка, которые
     * являются объектами типа Human или его подтипами.
     */
    public static int countOfHumans(@NotNull Object ... objects){
        int res = 0;
        for(Object temp: objects){
            if(temp instanceof Person){res++;}
        }
        return res;
    }

    /**
     * 2.Метод для объекта получает список имен его открытых методов.
     */
    @NotNull
    public static List<String> methodsOfObject(@NotNull Object obj){
        Method[] meths = obj.getClass().getMethods();
        List<String> res = new ArrayList<>();
        for(Method temp: meths){
            if(temp.getDeclaringClass().equals(obj.getClass())){
                res.add(temp.getName());
            }
        }
        return res;
    }

    /**
     * 3. Метод для объекта получает список (в виде списка строк) имен всех его суперклассов до класса
     * Object включительно.
     */
    @NotNull
    public static List<String> allSuperClassesOfObject(@NotNull Object obj){
        List <String> res = new ArrayList<>();
        @SuppressWarnings("rawtypes") Class tempClass = obj.getClass().getSuperclass();
        while (tempClass != null){
            res.add(tempClass.getSimpleName());
            tempClass = tempClass.getSuperclass();
        }
        return res;
    }

    /**
     * 4. Метод для списка объектов находит его элементы, реализующие интерфейс Executable,
     * и выполняет в таких объектах метод execute(). Метод возвращает количество
     * найденных элементов.
     */
    @Contract(pure = true)
    public static int countOfInterface(@NotNull Object ... objects){
        int result = 0;
        for(Object temp: objects){
            if(temp instanceof Executable){
                ((Executable) temp).execute();
                result++;
            }
        }
        return result;
    }

    /**
     * 5.Метод для объекта получает список его геттеров и сеттеров (в виде списка строк). Геттером
     * считаем открытый нестатический метод без параметров, значение которого не void, а имя
     * начинается с get. Сеттером считаем открытый нестатический метод с одним параметром, с
     * результатом типа void, а имя метода начинается с set.
     */
    @NotNull
    public static List<String> getObjectSettersGetters(@NotNull Object obj){
        Method[] methods = obj.getClass().getDeclaredMethods();
        List<String> res = new ArrayList<>();
        for(Method temp: methods){
            if (temp.getName().startsWith("get") && temp.getParameterCount() == 0 &&
                    (temp.getModifiers() & Modifier.PUBLIC) != 0 &&
                    (temp.getModifiers() & Modifier.STATIC) == 0){
                res.add(temp.getName());
            }
            else if(temp.getName().startsWith("set") && temp.getParameterCount() == 1 &&
                    (temp.getModifiers() & Modifier.PUBLIC) != 0 &&
                    (temp.getModifiers() & Modifier.STATIC) == 0) {
                res.add(temp.getName());}
        }
        return res;
    }


}
