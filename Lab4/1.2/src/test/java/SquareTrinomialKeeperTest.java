import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTrinomialKeeperTest {

    @Test
    void testGetGreaterRootWithCorrectParameter() {
        SquareTrinomialKeeper test = new SquareTrinomialKeeper(1,2,1);
        SquareTrinomialKeeper test1 = new SquareTrinomialKeeper(1,-3,2);
        assertEquals(-1,test.getGreaterRoot());
        assertEquals(2, test1.getGreaterRoot());
    }

    @Test
    void testGetGreaterRootWithIncorrectParameter() {
        SquareTrinomialKeeper test = new SquareTrinomialKeeper(1,2,5);
        assertThrows(IllegalArgumentException.class, test::getGreaterRoot);

    }

}