import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class HouseSerialize extends StdSerializer<House> {

    public HouseSerialize(){
        super(House.class);
    }
    @Override
    public void serialize(@NotNull House house, @NotNull JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("cadastralNumber", house.getCadastralNumber());
        jsonGenerator.writeStringField("address", house.getAddress());
        jsonGenerator.writeObjectField("housewife", house.getHouseWife());
        jsonGenerator.writeArrayFieldStart("apartments");
        for(Flat temp: house.getApartments()){
            jsonGenerator.writeObject(temp);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
