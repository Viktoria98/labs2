/**
 * Класс - компаратор для матриц, который сравнивает определители матриц.
 * @param <T>
 */

public class Comparator<T extends Matrix> implements java.util.Comparator<Matrix> {
    @Override
    public int compare(Matrix matrix1, Matrix matrix2) {
        try {
            return (int) (matrix1.getDeterminant() - matrix2.getDeterminant());
        }
        catch (MatrixException exc){
            return 0;
        }
    }
}
