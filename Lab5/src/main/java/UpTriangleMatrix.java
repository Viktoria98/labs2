/**
 * Класс UpTriangleMatrix (верхнетреугольная матрица). Размерность матрицы
 * также задается при ее создании и далее не меняется
 */
public class UpTriangleMatrix extends Matrix{

    UpTriangleMatrix(int N, double ... UpTriangleElems) throws MatrixException {
        super(N);
        if(UpTriangleElems.length != ((1+N)/2.)*N){
            throw new MatrixException("Слишком много/элементов для заполнения верхнетреугольной матрицы");
        }
        int k = 0;
        for(int i = 0; i < N; i++){
            for(int j = i; j < N; j++){
                editElem(i,j, UpTriangleElems[k]);
                k++;
            }
        }
    }

    @Override
    public void editElem(int i, int j, double elem) throws MatrixException {
        if(i >= getMatrixSize() || i < 0){
            throw new MatrixException("Некоректный индекс массива", i);
        }
        else if(j >= getMatrixSize() || j < 0){
            throw new MatrixException("Некоректный индекс массива", j);
        }
        else if(i > j){
            throw new MatrixException("Некоректный индекс для верхнетреугольной матрицы");
        }
        super.editElem(i, j, elem);
    }

}
