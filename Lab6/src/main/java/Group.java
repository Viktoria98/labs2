/**
 * Класс Group (группа данных). Класс должен содержать идентификатор
 * группы (целое число) и сами данные (массив целых чисел).
 */
public class Group {
    private int identifier;
    private int[] data;

    public Group(int identifier, int ... elems){
        this.identifier = identifier;
        data = elems;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public int[] getData() {
        return data;
    }

    public int getElem(int index) {
        return data[index];
    }

    public void setData(int[] data) {
        this.data = data;
    }

    public int getLength(){
        return data.length;
    }
}
