import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiagonalMatrixTest {

    @Test
    void testConstructor() throws MatrixException {
        assertThrows(MatrixException.class, ()-> new DiagonalMatrix(3, new double[]{1, 2, 3, 4}));
        DiagonalMatrix matrix = new DiagonalMatrix(4, new double[]{7,3,5,2});
    }

    @Test
    void editElem() throws MatrixException {
        DiagonalMatrix matrix = new DiagonalMatrix(4, new double[]{7,3,5,2});
        assertThrows(MatrixException.class, ()->matrix.editElem(1,2, 4));
        matrix.editElem(3,3, 11);
        assertEquals(11, matrix.getElem(3,3));
    }
}